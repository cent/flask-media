Configuration
=============

The following configuration values exist for :mod:`Flask-Media`.
:mod:`Flask-Media` loads these values from your main Flask config which can
be populated in various ways.  Note that some of those cannot be modified
after the engine was created so make sure to configure as early as
possible and to not modify them at runtime.

Configuration Keys
------------------

A list of configuration keys currently understood by the extension:

.. tabularcolumns:: |p{6.5cm}|p{8.5cm}|

================================== =========================================
``MEDIA_SETS``                     The list names mediasets space separated.
                                   Examples: `'avatar logo doc'`
``MEDIA_HANDLER``                  Enable handle for download uploaded files.
                                   Default: ``True``
``MEDIA_URL``                      If you have a server set up to serve the
                                   files in this set, this should be the URL
                                   they are publicly accessible from.
                                   Include the trailing slash. Default: `'/media'`
``MEDIA_PATH``                     Relative path when placed mediasets. Default: `'media'`
``MEDIA_ALLOW``                    The list extensions files allowed to store in
                                   mediaset. Default: allow all files.
``MEDIA_DENY``                     The list extensions files deny to store in
                                   mediaset. Default: not deny.
``MEDIA_SPLIT``                    Split destination mediaset dir on subdirs.
                                   Default: `0`
``MEDIA_THUMB_PATH``               Name thumbnails dirs for image. Default: `'thumb'`
================================== =========================================

All configuration keys may have suffix in uppercase name mediaset. For example,
`MEDIA_ALLOW_AVATAR` - allow extension for mediaset `avatar`.
