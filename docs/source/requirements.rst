Requirement
===========
* `Flask>=0.10`
* `SQLAlchemy>=1.0` (not mandatory)
* `Pillow>=2.9.0` (not mandatory)

Packages :mod:`SQLAlchemy` needed for support field :class:`MediaType` and :mod:`Pillow` needed for create
thumbnails.
