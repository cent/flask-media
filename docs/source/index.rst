===========
Flask-Media
===========

:mod:`Flask-Media` is an extension for :mod:`Flask` that allows your application upload files to server
and manage it. For image files supports create thumbnails. It requires :mod:`Flask` only.

.. toctree::
    :maxdepth: 1

    requirements
    install
    quickstart
    configuration
    api


