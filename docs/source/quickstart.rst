Quick start
===========


For use :mod:`Flask-Media` your must import class :class:`Media` from package :mod:`flask.ext.media`,
create extension and set set configuration::

    from flask import Flask
    from flask.ext.media import Media

    app = Flask(__name__)

    app.config['MEDIA_SETS'] = 'avatar'

    media = Media(app)

after your can use mediaset `avatar`::

    avatar_mediaset = media['avatar']

Method `save` mediaset return object :class:`MediaFile`::

    media_file = avatar_mediaset.save(request.files.get('file'))

object :class:`MediaFile` has same attribute::

    url = media_file.url  # Network access to file
    path = media_file.path  # Filesystem access to file

and methods::

    media_file.remove()  # remove file from mediaset

