# coding: utf-8
import os
from flask import Flask, render_template, redirect, flash, request, url_for, send_from_directory, abort
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.media import Media
from flask.ext.media.models import MediaType
from flask.ext.media.extesions import IMAGES, DOCUMENTS
from flask.ext.media.mediasets import UploadNotAllowed, MediaSetNotConfigured

__author__ = 'vadim'


instance_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'data')

if not os.path.exists(instance_path):
    os.makedirs(instance_path)

app = Flask('__name__', instance_path=instance_path)

app.config['SECRET_KEY'] = os.urandom(24)

# default Flask-Media config
app.config['MEDIA_URL'] = '/media'  # default prefix url
app.config['MEDIA_PATH'] = 'media'  # default path in instance folder
app.config['MEDIA_SPLIT'] = 0  # default split path on subdir

# define media sets
app.config['MEDIA_SETS'] = 'avatar logo document'

# configure media sets
# avatar media set
app.config['MEDIA_ALLOW_AVATAR'] = IMAGES

# logo media set
app.config['MEDIA_ALLOW_LOGO'] = IMAGES

# document media set
app.config['MEDIA_ALLOW_DOCUMENT'] = DOCUMENTS
app.config['MEDIA_URL_DOCUMENT'] = '/doc'  # set another prefix url
app.config['MEDIA_PATH_DOCUMENT'] = 'doc'  # set another path in instance folder

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///%s/test.db' % instance_path

media = Media(app)

db = SQLAlchemy(app)


class User(db.Model):
    """
    Example model with media field
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    avatar = db.Column(MediaType(255, mediaset='avatar'))


@app.route('/')
def index():
    """
    Handler main page
    :return:
    """
    return render_template('index.html', media=media, users=User.query)


@app.route('/upload/<string:mediaset>', methods=['POST'])
def upload(mediaset):
    """
    Handler upload file to mediaset
    :param mediaset: name mediaset
    :return:
    """
    try:
        ms = media[mediaset]
        ms.save(request.files.get('file'))
        flash('file uploaded to %s mediaset' % mediaset)
    except MediaSetNotConfigured:
        flash('mediaset not configured')
    except UploadNotAllowed:
        flash('file type not allowed')
    return redirect(url_for('index'))


@app.route('/delete/<string:mediaset>/<string:key>', methods=['POST'])
def delete(mediaset, key):
    """
    Handler delete file from mediaset
    :param mediaset: name mediaset
    :param key: relative path file
    :return:
    """
    try:
        media_file = media[mediaset][key]
        media_file.remove()
        flash('file removed from %s mediaset' % mediaset)
    except MediaSetNotConfigured:
        flash('mediaset not configured')
    return redirect(url_for('index'))


@app.route('/doc/<path:filename>')
def download_document(filename):
    """
    Handler custom download file
    :param filename: download file
    :return:
    """
    try:
        ms_file = media['document'][filename]
        filepath = ms_file.path
        return send_from_directory(os.path.dirname(filepath), os.path.basename(filepath))
    except MediaSetNotConfigured:
        abort(400)


@app.route('/upload_model/avatar', methods=['POST'])
def upload_to_model():
    """
    Handler upload file to media field sqlalchemy ORM Model
    :return:
    """
    try:
        user = User(name=request.form.get('name'))
        user.avatar = request.files.get('file')
        db.session.add(user)
        db.session.commit()
        flash('file uploaded to avatar mediaset')
    except MediaSetNotConfigured:
        flash('mediaset not configured')
    except UploadNotAllowed:
        flash('file type not allowed')
    return redirect(url_for('index'))


@app.route('/delete_model/avatar/<string:key>', methods=['POST'])
def delete_from_model(key):
    """
    Handler remove row with media field
    :param key:
    :return:
    """
    try:
        user = User.query.get(key)
        db.session.delete(user)
        db.session.commit()
        flash('file removed from avatar mediaset')
    except MediaSetNotConfigured:
        flash('mediaset not configured')
    return redirect(url_for('index'))


if __name__ == "__main__":
    db.create_all()
    app.run(debug=True)
